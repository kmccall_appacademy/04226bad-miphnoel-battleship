class HumanPlayer
  attr_reader :board, :name, :ships

  def initialize(name, board=Board.new)
    @name = name
    @board = board
    @ships = nil
  end


  def get_play
    puts "Where shall we strike, sir?"
    gets.chomp.split(',').map(&:to_i)
  end

  def place_ships(ships)
    @ships = ships

    ships.each do |ship|
      board.setup_display
      place_ship(ship)
    end
  end

  def place_ship(ship)
    start_pos = get_start_pos(ship)
    orientation = get_orientation(ship)
    coordinates = ship.gen_coordinates(start_pos, orientation)

    board.place_ship(coordinates)
  rescue
    puts "Our #{ship.name} doesn't fit there!"
    place_ship(ship)
  end

  def get_start_pos(ship)
    puts "Where should we place the top/left of the #{ship.name}? (length: #{ship.size})"
    start_pos = gets.chomp.split(',').map(&:to_i)
    raise "invalid" unless board.in_range?(start_pos)
    start_pos
  rescue
    puts "Please input in the form of 'row,column' (ex. '3,4')"
    get_start_pos(ship)
  end

  def get_orientation(ship)
    puts "Should the #{ship.name} be vertical or horizontal? (v/h)"
    orientation = gets.chomp.downcase
    raise "invalid" unless orientation == "v" || orientation == "h"
    orientation
  rescue
    puts "Please input in the form of 'v' or 'h'"
    get_orientation(ship)
  end

end
