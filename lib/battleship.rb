require_relative 'board'
require_relative 'player'
require_relative 'computer'
require_relative 'ship'
require 'pry'

class BattleshipGame
  attr_reader :current_player, :opponent, :player_one, :player_two

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = player_two
    @opponent = player_one
  end

  def self.default_ships
    carrier = Ship.new("Aircraft Carrier", 5)
    battleship = Ship.new("Battleship", 4)
    submarine = Ship.new("Submarine", 3)
    cruiser = Ship.new("Cruiser", 3)
    destroyer = Ship.new("Destroyer", 2)
    [carrier, battleship, submarine, cruiser, destroyer]
  end

  def play
    intro
    place_ships
    take_turns until game_over?
    outro
  end

  def game_over?
    opponent.board.won?
  end

  def take_turns
    switch_players!
    sleep(3)
    play_turn
  end

  def play_turn
    display_turn_status
    sleep(1)
    attack_phase
  end

  def display_turn_status
    puts "\n_____________________________________"
    puts "#{current_player.name.upcase}'s turn"
    puts "#{current_player.name.upcase}'s target area:"
    opponent.board.display
    puts "\nEnemy ships remaining: #{ships_remaining(@opponent)}"
    puts "Your ships remaining: #{ships_remaining(@current_player)}\n\n"
  end

  def attack_phase
    attack(choose_target)
  end

  def ships_remaining(player)
    live_ships = player.ships.reject { |ship| ship.sunk?(player.board) }
    live_ships.map(&:name).join(', ')
  end

  def attack(pos)
    puts "#{current_player.name} fires at: (#{pos.join(', ')})"
    opponent.board[pos].nil? ? miss(pos) : hit(pos)
  end

  def miss(pos)
    puts "Miss"
    opponent.board[pos] = :O
  end

  def hit(pos)
    puts "HIT!"
    opponent.board[pos] = :X
    display_if_sunk(pos)
  end

  def choose_target
    target = current_player.get_play
    raise "invalid" unless opponent.board.valid?(target)
    target
  rescue
    choose_another_target
  end

  def count
    opponent.board.count
  end

  def display_if_sunk(pos)
    sunken_ship = opponent.ships.detect do |ship|
      ship.is_at?(pos) && ship.sunk?(opponent.board)
    end
    puts "#{sunken_ship.name.upcase} SUNK!" if sunken_ship
  end

  def choose_another_target
    puts "That is not a valid target"
    choose_target
  end

  def switch_players!
    if current_player == player_one
      @current_player = player_two
      @opponent = player_one
    else
      @current_player = player_one
      @opponent = player_two
    end
  end

  def place_ships
    @player_one.place_ships(BattleshipGame.default_ships)
    @player_two.place_ships(BattleshipGame.default_ships)
  end

  def intro
    puts "BATTLESHIP: Sink or get sunk."
    puts "Each player will first have a chance to place their ships,"
    puts "then take turns firing at coordinates until one player has no ships left."
    puts "Enter coordinates in the form of 'row, column' (ex. 1, 3)\n\n"
  end

  def outro
    puts "#{opponent.name}'s last ship has been sunk! #{current_player.name} wins!"
  end
  # pry.binding
end

if __FILE__ == $PROGRAM_NAME
  mike = HumanPlayer.new("Mike")
  comp = ComputerPlayer.new
  game = BattleshipGame.new(mike, comp)
  game.play
end
