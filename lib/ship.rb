class Ship
  attr_reader :size, :coordinates, :name

  def initialize(name, size)
    @name = name
    @size = size
    @coordinates = nil
  end

  def gen_coordinates(start_pos, orientation)
    coordinates = []
    pos = start_pos

    size.times do
      coordinates << pos
      pos = increment_by_orientation(pos, orientation)
    end

    @coordinates = coordinates
  end

  def is_at?(pos)
    coordinates.include?(pos)
  end

  def increment_by_orientation(pos, orientation)
    row, col = pos
    return [row + 1, col] if orientation == "v"

    [row, col + 1]
  end

  def sunk?(board)
    coordinates.all? { |pos| board.attacked?(pos) }
  end
end
