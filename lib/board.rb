class Board
  attr_reader :grid

  def initialize(grid=Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def [](pos)
    row, column = pos
    grid[row][column]
  end

  def []=(pos, mark)
    row, column = pos
    grid[row][column] = mark
  end

  def count
    grid.flatten.count(:s)
  end

  # if no position provided, return whether board is empty of ships
  def empty?(pos=nil)
    pos.nil? ? count.zero? : self[pos].nil?
  end

  def full?
    grid.flatten.none?(&:nil?)
  end

  def place_random_ship
    raise "board is full" if full?
    self[empty_positions.sample] = :s
  end

  def place_ship(coordinates)
    raise "invalid" unless coordinates.all? do |pos|
      empty_positions.include?(pos)
    end
    coordinates.each do |pos|
      self[pos] = :s
    end
  end

  def populate_grid
    place_random_ship until full?
  end

  def won?
    count.zero?
  end

  def flexible_display(rows)
    puts "\n   #{(0..9).to_a.join('   ')}"
    rows.each_with_index do |row, idx|
      puts "#{idx}| #{row.join(' | ')}\n\n"
    end
  end

  def display
    flexible_display(rows_for_turn_display)
  end

  def setup_display
    flexible_display(rows_for_setup_display)
  end

  def empty_positions
    all_positions.select { |pos| empty?(pos) }
  end

  def all_positions
    positions = []
    grid.each_index do |row_idx|
      grid.size.times do |col_idx|
        pos = [row_idx, col_idx]
        positions << pos
      end
    end

    positions
  end

  def rows_for_setup_display
    # pos_rows = all_positions.each_slice(10).to_a
    #
    # pos_rows.map do |row|
    #   row.map do |pos|
    #     self[pos] == :s ? '(S)' : "#{pos[0]},#{pos[1]}"
    #   end
    # end
    grid.map do |row|
      row.map do |space|
        space = space.nil? ? '_' : 'S'
        space
      end
    end
  end

  def valid?(target)
    in_range?(target) && !attacked?(target)
  end

  def valid_targets
    all_positions.reject { |pos| attacked?(pos) }
  end

  def in_range?(pos)
    all_positions.include?(pos)
  end

  def attacked?(pos)
    [:X, :O].include?(self[pos])
  end

  def rows_for_turn_display
    grid.map do |row|
      row.map do |space|
        space = '_' if [:s, nil].include?(space)
        space
      end
    end
  end
end
