class ComputerPlayer
  attr_reader :board, :name, :ships

  def initialize(name="The Admiral", board=Board.new)
    @name = name
    @board = board
    @ships = nil
  end

  def get_play
    board.valid_targets.sample
  end

  def place_ships(ships)
    @ships = ships
    ships.each do |ship|
      place_ship(ship)
    end
  end

  def place_ship(ship)
    orientation = ['v', 'h'].sample
    start_pos = board.empty_positions.sample
    coordinates = ship.gen_coordinates(start_pos, orientation)
    board.place_ship(coordinates)
  rescue
    place_ship(ship)
  end
end
